
const originalArray = [1, [2, 3], 4, [5, [6, 7]]];


const originalObject = {
  a: 1,
  b: {
    c: 2,
    d: {
      e: 3,
      f: 4
    }
  }
};
// shallow copy
let shallowArray =  [...originalArray];


let shallowObject = {
    ...originalObject
};

// deep copy
let deepArray = JSON.parse(JSON.stringify(originalArray));

let deepObject = JSON.parse(JSON.stringify(originalObject));


//shallow modifying
shallowArray[0] =9;

shallowArray[1][0] = 20;

shallowObject.a = 10;

shallowObject.b.c = 50;

//object modyfying
deepArray[0]= 100;

deepArray[1][1] = 80;

deepObject.a = 90;

deepObject.b.d.e = 30;

console.log('original array: ',originalArray);
console.log('shallow array: ',shallowArray);
console.log('deep array: ',deepArray);
console.log('original object: ',originalObject);
console.log('shallow object: ',shallowObject);
console.log('deep object: ',deepObject);



