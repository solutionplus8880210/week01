let firstArr = [1, 2, 3];
let secondArr = [3, 4, 5];
let thirdArr = [5, 6, 7];


let spreadArr = [...firstArr, ...secondArr,...thirdArr];
console.log(spreadArr);

let uniqueArr = [...new Set(spreadArr)];
console.log(uniqueArr);