function calculateAverage (...numbers){
    if(numbers.length === 0){
        return 0;
    }

    let sum = numbers.reduce((acc, current) => acc + current, 0);

    let average = sum / numbers.length;

    return average;
}

console.log("the Average of numbers is ", calculateAverage(1,2,3,4));
console.log("the Average of numbers is ", calculateAverage(10, 20, 30, 40, 50));
console.log("the Average of numbers is ", calculateAverage(5, 10, 15, 20, 25, 30));
console.log("the Average of numbers is ", calculateAverage(0));
console.log("the Average of numbers is ", calculateAverage());